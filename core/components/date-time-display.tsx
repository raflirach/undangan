import React from "react";

const DateTimeDisplay = ({ value, type }) => {
  return (
    <div>
      <p className="text-2xl md:text-4xl">{value}</p>
      <span className="">{type}</span>
    </div>
  );
};

export default DateTimeDisplay;
