import DateTimeDisplay from "./date-time-display";

const ShowCounter = ({ days, hours, minutes, seconds }) => {
  return (
    <div className="show-counter">
      <div className="flex gap-4 md:gap-8 text-center">
        <DateTimeDisplay value={days} type={"Days"} />
        <DateTimeDisplay value={hours} type={"Hours"} />
        <DateTimeDisplay value={minutes} type={"Mins"} />
        <DateTimeDisplay value={seconds} type={"Seconds"} />
      </div>
    </div>
  );
};

export default ShowCounter;
