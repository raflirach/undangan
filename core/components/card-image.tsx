import React, { useRef } from "react";
import Image from "next/image";

export default function CardImage({ src }) {
  const handleOnClick = () => {
    document.getElementById("image").requestFullscreen();
  };

  return (
    <>
      <div
        className="group/image flex justify-center items-center relative w-full h-[36rem] cursor-pointer overflow-hidden"
        onClick={handleOnClick}
      >
        <Image
          id="image"
          src={src}
          alt="image-profile"
          fill
          className="object-cover  group-hover/image:scale-150 transition duration-500"
        />
        <div className="absolute opacity-0 group-hover/image:opacity-50 z-10 bg-primary w-full h-full"></div>
        <div className="z-10 border border-white text-white">View</div>
      </div>
    </>
  );
}
