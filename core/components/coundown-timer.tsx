import React from "react";
import { useCountdown } from "../utils/use-countdown";
import ShowCounter from "./show-counter";

const CountdownTimer = ({ targetDate }) => {
  const [days, hours, minutes, seconds] = useCountdown(targetDate);

  if (days + hours + minutes + seconds <= 0) {
    return (
      targetDate && <ShowCounter days={0} hours={0} minutes={0} seconds={0} />
    );
  } else {
    return (
      targetDate && (
        <ShowCounter
          days={days}
          hours={hours}
          minutes={minutes}
          seconds={seconds}
        />
      )
    );
  }
};

export default CountdownTimer;
