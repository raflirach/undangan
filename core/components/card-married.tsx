import React from "react";
import Image from "next/image";

export default function CardMarried({ path, name, desc }) {
  return (
    <div className="flex flex-col justify-center items-center">
      <Image
        className="rounded-t-full z-10"
        src={path}
        alt="image-profile"
        width={200}
        height={100}
      />
      <h1 className="text-xl md:text-2xl text-center mt-4 my-2 font-rubik z-10">
        {name}
      </h1>
      <p className="text-center text-lg md:text-xl font-rubik z-10">{desc}</p>
    </div>
  );
}
