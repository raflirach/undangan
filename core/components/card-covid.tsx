import React from "react";
import Image from "next/image";
import { useInView } from "react-intersection-observer";

export default function CardCovid({ path, text, view }) {
  const { ref, inView: inView } = useInView({
    threshold: 0,
  });
  const { ref: ref2, inView: inView2 } = useInView({
    threshold: 0,
  });
  return (
    <div
      ref={ref}
      className={`${view ? "animate__animated animate__zoomInUp animate__slow" : ""}
        flex flex-col p-4 items-center justify-center bg-white  h-50 rounded-lg`}
    >
      <Image
        className={``}
        src={path}
        alt="Picture of the author"
        width={100}
        height={100}
      />
      <h3
        className={`mt-4 text-black text-center`}
      >
        {text}
      </h3>
    </div>
  );
}
