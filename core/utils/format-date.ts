import moment from "moment";

export const dateGetDay = (date: string): string => {
  let newDate = new Date(date);
  const day = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
  return day[newDate.getDay()];
};
export const dateGetDate = (date: string): string => {
  let newDate = new Date(date);
  return newDate.getDate().toString();
};

export const dateGetMonth = (date: string): string => {
  let newDate = new Date(date);
  const month = [
    "Januari",
    "Februari",
    "Maret",
    "April",
    "Mei",
    "Juni",
    "Juli",
    "Agustus",
    "September",
    "Oktober",
    "November",
    "Desember",
  ];
  return month[newDate.getMonth()];
};

export const dateGetYear = (date: string): string => {
  let newDate = new Date(date);
  return newDate.getFullYear().toString();
};

export const getTargetDate = (date: string): number => {
  const now = new Date().toLocaleDateString("id-ID");
  const NOW_IN_MS = new Date().getTime();

  var today = moment(now, "D/M/YYYY");
  var target_date = moment(date, "YYYY-MM-DD");

  const diffDays = target_date.diff(today, "days");

  return NOW_IN_MS + diffDays * 24 * 60 * 60 * 1000;
};
