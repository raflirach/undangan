import React, { useCallback, useState } from "react";
import Image from "next/image";
import CardImage from "../../core/components/card-image";
import ImageViewer from "react-simple-image-viewer";
import zIndex from "@mui/material/styles/zIndex";

export default function SectionImages() {
  const images = [
    "/images/gallery/image-one.jpg",
    "/images/gallery/image-two.jpg",
    "/images/gallery/image-three.jpg",
    "/images/gallery/image-four.jpg",
    "/images/gallery/image-five.jpg",
    "/images/gallery/image-six.jpg",
  ];
  const [currentImage, setCurrentImage] = useState(0);
  const [isViewerOpen, setIsViewerOpen] = useState(false);
  const openImageViewer = useCallback((index) => {
    setCurrentImage(index);
    setIsViewerOpen(true);
  }, []);

  const closeImageViewer = () => {
    setCurrentImage(0);
    setIsViewerOpen(false);
  };

  return (
    <div className="py-16 grid grid-cols md:grid-cols-3 gap-2 md:px-8">
      {images.map((src, index) => (
        <div key={index} className="group/image flex justify-center items-center relative w-full h-[36rem] cursor-pointer overflow-hidden">
          <Image
            src={src}
            onClick={() => openImageViewer(index)}
            alt="image-profile"
            fill
            className="object-cover group-hover/image:scale-150 transition duration-500"
          />
        </div>
      ))}
      {isViewerOpen && (
        <ImageViewer
          src={images}
          currentIndex={currentImage}
          disableScroll={true}
          closeOnClickOutside={true}
          onClose={closeImageViewer}
          backgroundStyle={{
            backgroundColor: "rgba(0, 0, 0, 0.8)",
            zIndex: "100",
          }}
        />
      )}
    </div>
  );
}
