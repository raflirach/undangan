import React, { useCallback, useState } from "react";
import { useInView } from "react-intersection-observer";
import Image from "next/image";
import ImageViewer from "react-simple-image-viewer";

export default function SectionOurLoveStory() {
  const story = [
    "/images/our-love-story/image-one.jpeg",
    "/images/our-love-story/image-two.jpeg",
    "/images/our-love-story/image-three.jpeg"
  ]

  const [currentImage, setCurrentImage] = useState(0);
  const [isViewerOpen, setIsViewerOpen] = useState(false);
  const openImageViewer = useCallback((index) => {
    setCurrentImage(index);
    setIsViewerOpen(true);
  }, []);

  const closeImageViewer = () => {
    setCurrentImage(0);
    setIsViewerOpen(false);
  };

  const { ref, inView: inView } = useInView({
    threshold: 0,
  });

  const { ref: ref2, inView: inView2 } = useInView({
    threshold: 0,
  });

  const { ref: ref3, inView: inView3 } = useInView({
    threshold: 0,
  });

  const { ref: ref4, inView: inView4 } = useInView({
    threshold: 0,
  });

  const { ref: ref5, inView: inView5 } = useInView({
    threshold: 0,
  });

  return (
    <>
      <div className="py-16 bg-primary font-playfair text-white space-y-10">
        <div ref={ref} className="text-center space-y-1 px-3">
          <h2
            className={`${inView ? "animate__animated animate__fadeInDown" : ""} 
          font-bold text-3xl`}
          >
            Kisah Perjalanan Cinta
          </h2>
        </div>

        <div
          ref={ref2}
          className="grid grid-cols-1 gap-y-10 md:grid-cols-2 md:mx-24"
        >
          <div
            className={`${
              inView2 ? "animate__animated animate__backInUp animate__slow" : ""
            } 
          px-3 md:px-5 md:order-2`}
          >
            {/* <Image
              src={story[0]}
              onClick={() => openImageViewer(0)}
              alt="image-profile"
              fill
              className="object-cover w-full max-h-[30rem] rounded-md md:h-[20rem] md:rounded-xl"
            /> */}
            <img
              src={story[0]}
              onClick={() => openImageViewer(0)}
              alt="logo"
              className="cursor-pointer object-cover w-full max-h-[30rem] rounded-md md:h-[20rem] md:rounded-xl"
            />
          </div>
          <div
            className={`${
              inView2 ? "animate__animated animate__backInUp animate__slow" : ""
            } 
          font-thin px-3 md:px-5 md:text-justify md:m-auto md:order-1`}
          >
            <h2 className="font-bold text-xl md:text-2xl mb-2">Awal Bertemu</h2>
            <p className="font-medium text-sm md:text-lg">
              Jum'at 2011, awal pertemuan kita, bulan nya lupa wkwk. Di waktu saya
              kelas 1 SMP dan Aa kelas 3 SMA, Di dekat warung pop ice depan rumah
              Aa.
            </p>
          </div>
        </div>
        <div
          ref={ref3}
          className="grid grid-cols-1 gap-y-10 md:grid-cols-2 md:mx-24"
        >
          <div
            className={`${
              inView3 ? "animate__animated animate__backInUp animate__slow" : ""
            }
            px-3 md:px-5`}
          >
            {/* <Image
              src={story[1]}
              onClick={() => openImageViewer(1)}
              alt="image-profile"
              fill
              className="object-cover w-full max-h-[30rem] rounded-md md:h-[20rem] md:rounded-xl"
            /> */}
            <img
              src={story[1]}
              onClick={() => openImageViewer(1)}
              alt="logo"
              className="cursor-pointer w-full max-h-[30rem] object-cover rounded-md md:h-[20rem] md:rounded-xl"
            />
          </div>
          <div
            className={`${
              inView3 ? "animate__animated animate__backInUp animate__slow" : ""
            }
          font-thin px-3 md:px-5 md:text-justify md:m-auto`}
          >
            <h2 className="font-bold text-xl md:text-2xl mb-2">
              Berkomitmen Bersama
            </h2>
            <p className="font-medium text-sm md:text-lg">
              Tepatnya 2017, Saya di Bandung dan Aa di Bogor. Singkat cerita Aa
              menetap di Bandung dan kita memutuskan untuk berkomitmen ke jenjang
              yang lebih serius untuk masa depan.
            </p>
          </div>
        </div>
        <div
          ref={ref4}
          className="grid grid-cols-1 gap-y-10 md:grid-cols-2 md:mx-24"
        >
          <div
            className={`${
              inView4 ? "animate__animated animate__backInUp animate__slow" : ""
            } 
            px-3 md:px-5 md:order-2`}
          >
            {/* <Image
              src={story[2]}
              onClick={() => openImageViewer(2)}
              alt="image-profile"
              fill
              className="object-cover w-full max-h-[30rem] rounded-md md:h-[20rem] md:rounded-xl"
            /> */}
            <img
              src={story[2]}
              onClick={() => openImageViewer(2)}
              alt="logo"
              className="cursor-pointer w-full max-h-[30rem] object-cover rounded-md md:h-[20rem] md:rounded-xl"
            />
          </div>
          <div
            className={`${
              inView4 ? "animate__animated animate__backInUp animate__slow" : ""
            } 
          font-thin px-3 md:px-5 md:text-justify md:m-auto md:order-1`}
          >
            <h2 className="font-bold text-xl md:text-2xl mb-2">Melamar</h2>
            <p className="font-medium text-sm md:text-lg">
              Tanggal 25/09/22 Aa membawa keluarganya bersilaturahmi ke rumah
              saya, serta meminta saya untuk menikah dengannya.
            </p>
          </div>
        </div>
      </div>
      {isViewerOpen && (
        <ImageViewer
          src={story}
          currentIndex={currentImage}
          disableScroll={true}
          closeOnClickOutside={true}
          onClose={closeImageViewer}
          backgroundStyle={{
            backgroundColor: "rgba(0, 0, 0, 0.8)",
            zIndex: "100",
          }}
        />
      )}
    </>
  );
}
