import React from "react";
import CountdownTimer from "../../core/components/coundown-timer";
import { useInView } from "react-intersection-observer";

export default function SectionSaveDate() {
  const { ref, inView: inView } = useInView({
    threshold: 0,
  });
  const { ref: ref2, inView: inView2 } = useInView({
    threshold: 0,
  });
  const { ref: ref3, inView: inView3 } = useInView({
    threshold: 0,
  });
  return (
    <div
      ref={ref}
      className="relative flex flex-col justify-center items-center gap-4 md:gap-8 bg-primary py-8 md:py-10 font-rubik"
    >
      <div
        ref={ref2}
        className={`${
          inView ? "animate__animated animate__fadeInLeft animate__slow" : ""
        } text-2xl md:text-3xl lg:text-3xl text-white`}
      >
        Jan 1<span className="text-sm">st</span> 2023
      </div>

      <img
        ref={ref3}
        src="/images/save-date/save-the-date.gif"
        alt="logo"
        className={`${
          inView2 ? "animate__animated animate__fadeInDown animate__slow" : ""
        } h-40 w-40 md:h-60 md:w-60 lg:h-60 lg:w-60`}
      />

      <div
        className={`${
          inView3 ? "animate__animated animate__zoomIn animate__slow" : ""
        } text-white`}
      >
        <CountdownTimer targetDate={"01/01/2023 11:00"} />
      </div>
    </div>
  );
}
