import dynamic from "next/dynamic";
import React from "react";
import { useInView } from "react-intersection-observer";
const ReactPlayer = dynamic(() => import("react-player"), { ssr: false });

export default function SectionVideo() {
  const { ref, inView: inView } = useInView({
    threshold: 0,
  });
  const { ref: ref2, inView: inView2 } = useInView({
    threshold: 0,
  });
  const { ref: ref3, inView: inView3 } = useInView({
    threshold: 0,
  });
  return (
    <div
      ref={ref}
      className="relative flex items-center justify-center my-8 md:my-16 px-2 md:px-16 lg:px-32"
    >
      <img
        ref={ref2}
        src="/images/video/video-image.png"
        alt="video-left"
        className={`${
          inView ? "animate__animated animate__zoomInRight animate__slow" : ""
        } absolute -left-20 md:-left-40 h-[35rem] md:h-[60rem]`}
      />
      <div
        ref={ref3}
        className="flex flex-col w-full h-full justify-center items-center gap-4 md:gap-8"
      >
        <div
          className={`${
            inView2 ? "animate__animated animate__fadeInDown animate__slow" : ""
          } text-2xl md:text-4xl font-bold font-playfair`}
        >
          Video Prewedding
        </div>
        <div
          className={`${
            inView3 ? "animate__animated animate__zoomIn animate__slow" : ""
          } h-[20rem] md:h-[30rem] lg:h-[35rem] w-full flex flex-col justify-center items-center gap-4 md:gap-8 md:px-20`}
        >
          <ReactPlayer
            url="https://www.youtube.com/watch?v=1Qw5Q07A8Co"
            width="100%"
            height="100%"
            controls={true}
          />
        </div>
      </div>
    </div>
  );
}
