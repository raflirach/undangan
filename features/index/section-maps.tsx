import React from "react";

export default function SectionMaps() {
  return <div className="flex justify-center py-10 bg-primary">
    <iframe 
      src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3960.621736802224!2d107.56165351434773!3d-6.935731669818941!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68ef8e84cb0d5f%3A0xf28903f61929f33!2sToko%20Naslun%20Saleh%20Grup!5e0!3m2!1sid!2sid!4v1670848361128!5m2!1sid!2sid" 
      width="800" 
      height="600" 
      className="w-full"
      style={{border: 0}} 
      allowFullScreen={true} 
      loading="lazy" 
      referrerPolicy="no-referrer-when-downgrade"></iframe>
  </div>;
}
