import React, {
  AudioHTMLAttributes,
  MutableRefObject,
  useRef,
  useState,
} from "react";
import MusicNoteRoundedIcon from "@mui/icons-material/MusicNoteRounded";
import MusicOffRoundedIcon from "@mui/icons-material/MusicOffRounded";

interface ReactRef<T> {
  current: null | T;
}

export default function SectionAudio() {
  const audioElement: ReactRef<any> = useRef();
  const [isPlay, setIsPlay] = useState(true);

  const handleOnClick = () => {
    setIsPlay(!isPlay);
    isPlay ? audioElement.current.pause() : audioElement.current.play();
  };

  return (
    <div className="fixed z-50 right-2 bottom-2">
      <button className="animate__animated animate__pulse animate__infinite" onClick={handleOnClick}>
        <svg className="w-8 h-8 md:w-12 md:h-12 bg-primary rounded-full p-1 md:p-2 text-white border border-white">
          {isPlay ? <MusicNoteRoundedIcon /> : <MusicOffRoundedIcon />}
        </svg>
      </button>

      <audio
        ref={audioElement}
        autoPlay
        src="/audio/Beautiful_In_White.mp3"
      ></audio>
    </div>
  );
}
