import React from "react";
import { useInView } from "react-intersection-observer";
import CardCovid from "../../core/components/card-covid";

export default function SectionCovid() {
  const { ref, inView: inView } = useInView({
    threshold: 0,
  });
  return (
    <section ref={ref} className="p-10 md:px-28 lg:px-44 bg-primary">
      <h1
        className={`${
          inView ? "animate__animated animate__zoomIn animate__slow" : ""
        } text-center text-3xl font-bold font-playfair mb-8 text-white`}
      >
        COVID-19 Protocols
      </h1>
      <div className="grid grid-cols-2 gap-4 lg:grid-cols-6">
        <CardCovid 
          view={inView}
          path="/icons/covid-wash-hand.svg" text="Wash Your Hands" />
        <CardCovid
          view={inView}
          path="/icons/covid-wear-mask.svg"
          text="Wear Mask Properly"
        />
        <CardCovid
          view={inView}
          path="/icons/covid-dont-handshake.svg"
          text="Do not Handshake"
        />
        <CardCovid view={inView} path="/icons/covid-keep-distance.svg" text="Keep Distance" />
        <CardCovid view={inView} path="/icons/covid-avoid-crowds.svg" text="Avoid Crowds" />
        <CardCovid
          view={inView}
          path="/icons/covid-body-temperature-check.svg"
          text="Temperature Check"
        />
      </div>
    </section>
  );
}
