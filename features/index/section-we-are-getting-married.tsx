import React from "react";
import CardMarried from "../../core/components/card-married";
import Image from "next/image";
import { useInView } from "react-intersection-observer";

export default function SectionWeAreGettingMarried() {
  const { ref, inView: inView } = useInView({
    threshold: 0,
  });

  const { ref: ref2, inView: inView2 } = useInView({
    threshold: 0,
  });

  const { ref: ref3, inView: inView3 } = useInView({
    threshold: 0,
  });

  const { ref: ref4, inView: inView4 } = useInView({
    threshold: 0,
  });

  return (
    <section ref={ref3} className="p-10 bg-secondary text-black font-rubik">
      <h1
        ref={ref}
        className={`${
          inView3 ? "animate__animated animate__zoomInUp animate__slow" : ""
        } text-3xl mb-8 text-center`}
      >
        Pasangan Mempelai
      </h1>
      <div className="flex flex-col md:flex-row items-center justify-center">
        <div
          ref={ref4}
          className={`${
            inView ? "animate__animated animate__zoomInLeft animate__slow" : ""
          } relative`}
        >
          <Image
            className="absolute -top-48 lg:-left-4 hidden md:block md:-left-24"
            src="/images/we-are-getting-married/flower-married-one.png"
            alt="Picture of the author"
            width={400}
            height={400}
          />
          <CardMarried
            path="/images/we-are-getting-married/female.jpg"
            name="Siti Sifa Nurrohmah"
            desc="Putri dari Bapak H. Agus dan Ibu Hj. Dede Hapsoh (Almrh)"
          />
        </div>
        <span
          ref={ref2}
          className={`${
            inView3 ? "animate__animated animate__zoomIn animate__slow" : ""
          } text-6xl md:text-[4em] lg:text-[5em] my-4`}
        >
          &
        </span>
        <div
          className={`${
            inView2
              ? "animate__animated animate__zoomInRight animate__slow"
              : ""
          } relative`}
        >
          <Image
            className="absolute -top-48 lg:-right-8 hidden md:block md:-right-14"
            src="/images/we-are-getting-married/flower-married-two.png"
            alt="Picture of the author"
            width={400}
            height={400}
          />
          <CardMarried
            path="/images/we-are-getting-married/male.jpg"
            name="Asep Ropi Adarojat"
            desc="Putra dari Bapak Agusman Safari dan Ibu Ani Suryani (Almrh)"
          />
        </div>
      </div>
    </section>
  );
}
