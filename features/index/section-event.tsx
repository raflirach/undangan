import React from "react";
import { useInView } from "react-intersection-observer";

export default function SectionEvent() {
  const { ref, inView: inView } = useInView({
    threshold: 0,
  });

  const { ref: ref2, inView: inView2 } = useInView({
    threshold: 0,
  });

  return (
    <div
      ref={ref}
      className="relative my-10 py-16 text-slate-500 overflow-hidden font-playfair"
    >
      <img
        className={`absolute -top-10 right-0 w-[250px] translate-x-[4rem] rotate-180
      md:top-0 md:-translate-y-[9rem] md:w-[400px]
      lg:top-0 lg:-translate-y-[7rem] lg:w-[600px] lg:-right-[10rem]`}
        alt="event"
        src="/images/event/event.png"
      />

      <img
        className={`absolute -bottom-16 w-[250px] -translate-x-[4rem]
      md:bottom-0 md:translate-y-[14rem] md:w-[400px]
      lg:bottom-0 lg:translate-y-[8rem] lg:w-[600px] lg:-left-[10rem]`}
        alt="event"
        src="/images/event/event.png"
      />
      <div ref={ref2} className="text-center space-y-12">
        <div className="space-y-2">
          <h2
            className={`${
              inView
                ? "animate__animated animate__backInDown animate__slow"
                : ""
            }
        text-4xl font-bold`}
          >
            Acara
          </h2>
          <div
            className={`${
              inView
                ? "animate__animated animate__fadeInDown animate__slow"
                : ""
            }
          font-merriweather font-medium`}
          >
            Minggu
          </div>
          <div className="flex justify-center font-merriweather">
            <div
              className={`${
                inView
                  ? "animate__animated animate__fadeInLeft animate__slow"
                  : ""
              }
            text-5xl font-extrabold`}
            >
              01
            </div>
            <div
              className={`${
                inView
                  ? "animate__animated animate__fadeInRight animate__slow"
                  : ""
              }`}
            >
              <div className="font-medium">Januari</div>
              <div className="font-medium">2023</div>
            </div>
          </div>
        </div>
        <div className="space-y-5 md:space-y-0 md:flex md:space-x-80 md:justify-center z-50">
          <div
            className={`${
              inView2
                ? "animate__animated animate__fadeInLeft animate__slow"
                : ""
            }
          col space-y-3`}
          >
            <h1 className={`text-4xl font-light`}>Akad Nikah</h1>
            <div className="font-medium">08:00 - Selesai</div>
            <div>
              <a
                target="_blank"
                rel="noreferrer"
                className="border border-primary py-2 rounded-lg px-4 text-primary hover:bg-primary hover:text-white"
                href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=NWIxcmFsMTY5aTQwMGdhZjYzZjdmZXV2ZHYgcml6a2lyYW1hNDZAbQ&amp;tmsrc=rizkirama46%40gmail.com"
              >
                Tambahkan ke Kalender
              </a>
            </div>
          </div>
          <div
            className={`${
              inView2
                ? "animate__animated animate__fadeInRight animate__slow"
                : ""
            }
          col space-y-3`}
          >
            <h1 className={`text-4xl font-light`}>Resepsi</h1>
            <div className="font-medium">11:00 - 16:00</div>
            <div>
              <a
                target="_blank"
                rel="noreferrer"
                className="border border-primary py-2 rounded-lg px-4 text-primary hover:bg-primary hover:text-white"
                href="https://calendar.google.com/calendar/event?action=TEMPLATE&amp;tmeid=MDFqN2RiZ2JyM3JqMDcxcXN2OW10bGQzbTUgcml6a2lyYW1hNDZAbQ&amp;tmsrc=rizkirama46%40gmail.com"
              >
                Tambahkan ke Kalender
              </a>
            </div>
          </div>
        </div>
        <div
          ref={ref2}
          className={`${
            inView2 ? "animate__animated animate__fadeInUp animate__slow" : ""
          }
        space-y-2`}
        >
          <h1 className="text-xl font-bold font-merriweather">
            Kediaman Mempelai Wanita
          </h1>
          <div className="font-medium">
            Jl. Cigondewah Kaler, Kec. Bandung Kulon
          </div>
          <div className="font-medium">Bandung</div>
          <div>
            <a
              target="_blank"
              rel="noreferrer"
              className="border border-primary py-2 rounded-lg px-4 text-primary hover:bg-primary hover:text-white"
              href="https://goo.gl/maps/AwBcseRdG2RZoHt2A"
            >
              Lihat Lokasi
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
