import { useRouter } from "next/router";
import React from "react";

export default function SectionOpeningInvitation({ open }) {
  const router = useRouter();
  const { to } = router.query;

  return (
    <div className="flex px-10 h-screen bg-openInvitation bg-center bg-cover font-merriweather">
      <div className="text-center text-white m-auto">
        <h3 className="font-semibold text-4xl mb-10 font-playfair animate__animated animate__pulse animate__infinite animate__slow">
          Sifa & Ropi
        </h3>
        <div className="font-medium font-playfair mb-3">
          Kepada Bapak/Ibu/Saudara/i
        </div>
        <div className="font-semibold text-2xl font-playfair mb-3">{to}</div>
        <div className="font-medium font-playfair mb-10">
          Kami Mengundang Anda Untuk Hadir Di Acara Pernikahan Kami
        </div>
        <button
          className="text-white bg-primary px-6 py-3 rounded-md mt-2 
          hover:text-primary hover:bg-white hover:border hover:border-primary
          animate__animated animate__pulse animate__infinite"
          onClick={() => open()}
        >
          Buka Undangan
        </button>
      </div>
    </div>
  );
}
