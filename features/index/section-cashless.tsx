import React, { useState } from "react";
import Image from "next/image";
import { useInView } from "react-intersection-observer";
import RedeemRoundedIcon from "@mui/icons-material/RedeemRounded";

export default function SectionCashless() {
  const [copied, setCopied] = useState(false);

  const handleCopy = async () => {
    try {
      await navigator.clipboard.writeText("400201036025533");
      setCopied(true);
      setTimeout(() => {
        setCopied(false);
      }, 5000);
    } catch (err) {
      console.error("Failed to copy: ", err);
    }
  };

  const { ref, inView: inView } = useInView({
    threshold: 0,
  });

  const { ref: ref2, inView: inView2 } = useInView({
    threshold: 0,
  });

  const { ref: ref3, inView: inView3 } = useInView({
    threshold: 0,
  });

  return (
    <div
      ref={ref}
      className="text-white flex flex-col items-center text-center bg-primary 
            space-y-8 py-16 px-5 font-merriweather"
    >
      <h2
        className={`${
          inView ? "animate__animated animate__backInDown" : ""
        } text-3xl font-bold font-playfair`}
      >
        Wedding Gift
      </h2>
      <div
        className={`${
          inView ? "animate__animated animate__backInUp animate__slow" : ""
        } md:text-xl font-medium`}
      >
        Doa Restu Anda merupakan karunia yang sangat berarti bagi kami.
      </div>
      <div
        ref={ref2}
        className={`${
          inView ? "animate__animated animate__backInDown animate__slow" : ""
        } 
        md:text-xl font-medium text-white `}
      >
        Dan jika memberi adalah ungkapan tanda kasih Anda, Anda dapat memberi
        kado secara cashless.
      </div>
      <div className="flex flex-col md:flex-row gap-10">
        <div
          className={`animate__animated animate__pulse animate__infinite animate__slow
            bg-white flex flex-col justify-center items-center p-5 rounded-lg w-full`}
        >
          <div className="h-10 flex items-center">
            <Image
              className="w-20"
              src="/icons/ic-bri.png"
              alt="Bank Logo"
              width={100}
              height={100}
            />
          </div>

          <span className="h-[0.15rem] w-full bg-primary rounded-full my-5"></span>
          <div className="">
            <div className="text-slate-700 text-sm md:font-semibold">
              No. Rekening: 400201036025533
            </div>
            <div ref={ref3} className="text-slate-700 text-sm md:font-semibold">
              A/n. Asep Ropi Adarojat
            </div>
            <button
              className="text-white bg-primary px-4 py-2 rounded-md mt-2
          hover:text-primary hover:bg-white hover:border hover:border-primary"
              onClick={handleCopy}
            >
              {copied ? "Tersalin!" : "Salin Rekening"}
            </button>
          </div>
        </div>
        <div
          className={`animate__animated animate__pulse animate__infinite animate__slow
          bg-white flex flex-col justify-center items-center p-5 rounded-lg w-full`}
        >
          <div
            className={`text-slate-700 font-bold text-xl h-12 flex flex-col justify-center items-center`}
          >
            <RedeemRoundedIcon />
            Kirim Hadiah
          </div>
          <span className="h-[0.15rem] w-full bg-primary rounded-full my-5"></span>
          <div className={`space-y-2`}>
            <div className="text-slate-700 text-sm md:font-semibold">
              Nama Penerima: Siti Sifa Nurrohmah{" "}
            </div>
            <div className="text-slate-700 text-sm md:font-semibold">
              No. Hp: 082121393745
            </div>
            <div className="text-slate-700 text-sm md:font-semibold">
              Alamat: Jl. Cigondewah Kaler RT/RW: 02/09, Kel. Cigondewah Kaler,
              Kec. Bandung Kulon.
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
