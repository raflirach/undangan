import Footer from "../core/components/footer";

import "animate.css";
import {
  SectionAudio,
  SectionCashless,
  SectionCovid,
  SectionEvent,
  SectionHero,
  SectionImages,
  SectionMaps,
  SectionOpeningInvitation,
  SectionOurLoveStory,
  SectionSaveDate,
  SectionVideo,
  SectionWeAreGettingMarried,
  Snow,
} from "../features/index";
import { useState } from "react";
import Head from "next/head";

export default function Home() {
  const [open, setOpen] = useState(false);

  const onOpen = () => setOpen(!open);

  return (
    <>
      <Head>
        <title>Pernikahan Sifa & Ropi</title>
        <link
          rel="apple-touch-icon"
          sizes="180x180"
          href="/favicon/apple-touch-icon.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="32x32"
          href="/favicon/favicon-32x32.png"
        />
        <link
          rel="icon"
          type="image/png"
          sizes="16x16"
          href="/favicon/favicon-16x16.png"
        />
        <link rel="manifest" href="/favicon/site.webmanifest" />
      </Head>
      <div>
        {!open ? (
          <SectionOpeningInvitation open={onOpen} />
        ) : (
          <main>
            <Snow />
            <SectionHero />
            <SectionWeAreGettingMarried />
            <SectionOurLoveStory />
            <SectionImages />
            <SectionSaveDate />
            <SectionEvent />
            <SectionMaps />
            <SectionVideo />
            <SectionCashless />
            <SectionCovid />
            <SectionAudio />
            <Footer />
          </main>
        )}
      </div>
    </>
  );
}
