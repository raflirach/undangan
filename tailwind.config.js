/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./core/**/*.{js,ts,jsx,tsx}",
    "./features/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        rubik: ["Rubik Vinyl", "cursive"],
        playfair: ["Playfair Display", "serif"],
        merriweather: ["Merriweather Sans", "sans-serif"],
      },
      colors: {
        primary: "#578ca9",
        primaryAscent: "#97bfd4",
        secondary: "#d8f1ff",
        secondaryAscent: "#b3d3e5",
      },
      backgroundImage: {
        openInvitation: "url('../public/images/opening/open-image.jpg')",
      },
    },
  },
  plugins: [],
};
